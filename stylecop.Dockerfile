FROM alpine:3.8

RUN apk add --no-cache mono --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing && \
    apk add --no-cache --virtual=.build-dependencies ca-certificates && \
    cert-sync /etc/ssl/certs/ca-certificates.crt && \
    apk del .build-dependencies
    
ADD StylecopBaboon.zip /stylecop/

RUN unzip /stylecop/StylecopBaboon.zip